(define-module (ice-9 set complement)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-complementable-set 
	    make-complementable-set-mac 
	    <set> set? Ω
	    
            ;;;Example complementable oredered set builed out of srfi-1 listsets
	    *set-equality-predicate*
	    with-lset-operators
	    with-tex-lset-operators
	    
	    *set-map-equality-predicate* 
	    with-lassoc-operators))

#|
FUNCTIONAL COMPLEMENTABLE SETS AND ORDERED SETS AND SETMAPS

This is a higher order library that will based on a programmable
set datatype without complement construct a library for doing set operations 
by taking complement without a universe defined.

This library treat complements without defining a world. Mathematically that
leads to nondecidability issues, but if one construct all such sets from 
factual discrete elements no technical problems will appear. To actually get 
a set define a world as the underlying set lib and intersect it with the 
complemented set, that results in the underlying set. Bitsets has lognot and
in guile (gmp) manages bitset complements with lognot and the minus sign. One can define complement for bitsets as well with this library but that's not 
nessesary the gmp has all the magic for this. 

All nessessry set operators are defined with complement feature included
The working code is using the fact that every complemented set that is 
constructed from atoms and set operations can be defined as A ⊔ Bᶜ. 
This is not unique definition, to make it unique one could specify that 
B ∖ A = B that is ̧A⊆Bᶜ and this is also what we constrain the complements 
to be. FOr unordered sets one can assume that A=∅. But when the order is 
importane one need to be more carefule and a more comple solution results.
Ordering is defined through the ordering of operands in X ∪ Y and X ∩ Y, 
that is in a serialisation of X op Y, then elements in sets in X comes befor 
elements if sets in Y. Ordered sets has a well defined order and can can seen 
as an assoc list e.g. we can consider mapings from key elements to values in 
which case lookup of mappings is through the ordered list and hence defines a 
more generalised lookup. complements of an ordered map sets have no map defined
for them, any such maps need to be taken from the world at instantiation of 
a complemented set into that world, or the real sets it is intersecting with.

(make-complementable-set ∅ union intersection difference triple) 
   -> (values #:world Ω #:u uSS #:n nSS #:c cS #:+ ⊕SS #:- ∖SS)) 
Input
union          A,B   -> {x|x∈A or  x∈B}               (A∪B)    
intersection   A,B   -> {x|x∈A and x∈B}               (A∩B)
difference     A,B   -> {x|x∈A and x∉B}               (A∖B)
triple         A,B,C -> {x|x∈A and (x∈B or x∉C)}      (A∩(B∪Cᶜ))
∅              the empty set
Ω = ∅⊔∅ᶜ == (make-set ∅ ∅) defines the world.

Output:
#:u    : ∪  - union for complemented sets         0,1,... arguments
#:n    : ∩  - intersection for complemented sets  0,1,... arguments
#:c    : *ᶜ  - set complement                     1 argument
#:-    : ∖  - set difference =  A∩Bᶜ              2 arguments
#:o    : ⊕  - set adition    = (A∖B ∪ B∖A)        0,1,.... arguments

We also export a set struct according to
($ <set> set complement)

An example complemented set is implemented this is the API
*set-equality-predicate*      

default equal?, a syntax parameter of the equality
predicate, a change of this will expand the code
using that predicate

(with-lset-operators code ...)
Execute the code ... with the following bounded functions in code ...
(lambda* (#:u ∪ #:n ∩ #:c c #:+ ⊕ #: ∖) code ...)

*set-map-equality-predicate* 
default equal?, a syntax parameter of the equality
predicate, a change of this will expand the code
using that predicate

(with-lassoc-operators code ...)

Assoc based maps
|#

(define-record-type <set>
  (make-set- set complement)
  set?
  (set set-set)
  (complement set-complement))

(set-record-type-printer! <set>
  (lambda (vl port) 
    (format port "#<~a ⊔ ~aᶜ>" (set-set vl) (set-complement vl))))

(define-syntax-rule (define-tool 
		      make-complementable-set 
		      make-complementable-set-mac
		      (args ...) code ...)
  (begin
    (define             (make-complementable-set     args ...) code ...)
    (define-syntax-rule (make-complementable-set-mac args ...) (begin code ...))))

(define-tool make-complementable-set make-complementable-set-mac
             (∅ union intersection difference equiv? set-size)
  
  (define Ω (make-set- ∅ ∅))


  (define (make-set x y)
    (if (and (set? x) (= 0 (set-size x)) (set? y) (= 0 (set-size y)))
	Ω
	(make-set- x y)))

  (define (norm x)
    (match x
      (($ <set> x y)
       (make-set x (difference y x)))
      (x x)))

  (define (ifΩ x)
    (match x 
      (($ <set> c d)
       (if (and (= (set-size c) 0) (= (set-size d) 0))
	   Ω
	   x))))

  (define uSSo
    (case-lambda
     (()  ∅)
     ((x) x)
     ((x y)
      (match (list x y)
	;;; (A⊔Bᶜ)∪(C⊔Dᶜ) = (A∪C)⊔(B∩D)ᶜ = X⊔Yᶜ, Y∖X=Y.
	((($ <set> a b) ($ <set> c d))
	 (ifΩ (make-set (union a c) (intersection b d))))

	;;; (A⊔Bᶜ)∪C = (A∪C)⊔(B∖C)ᶜ = X⊔Yᶜ, Y∖X=Y. 
	((($ <set> a b) c)
	 (ifΩ (make-set (union a c) (difference b c))))

	;;; A∪(C⊔Dᶜ) = (A∪C)⊔(D∖A)ᶜ = X⊔Yᶜ, Y∖X=Y.
	((a ($ <set> c d))
	 (ifΩ (make-set (union a c) (difference d a))))
	
	;;  A∪C
	((a c)
	 (union a c))))

     ((x y . l)
      (uSSo x (apply uSSo y l))))) 

  (define uSS
    (case-lambda
     (()  ∅)
     ((x) x)
     ((x y)
      (match (list x y)
	;;; (A⊔Bᶜ)∪(C⊔Dᶜ) = (A∪C)⊔(B∩D)ᶜ = X⊔Yᶜ, Y∖X=Y.
	((($ <set> a b) ($ <set> c d))
	 (ifΩ (make-set ∅ (intersection b d))))

	;;; (A⊔Bᶜ)∪C = (A∪C)⊔(B∖C)ᶜ = X⊔Yᶜ, Y∖X=Y. 
	((($ <set> a b) c)
	 (ifΩ (make-set ∅ (difference b c))))

	;;; A∪(C⊔Dᶜ) = (A∪C)⊔(D∖A)ᶜ = X⊔Yᶜ, Y∖X=Y.
	((a ($ <set> c d))
	 (ifΩ (make-set ∅ (difference d a))))
	
	;;  A∪C
	((a c)
	 (union a c))))

     ((x y . l)
      (uSS x (apply uSS y l))))) 

  (define nSS 
    (case-lambda
     (()  Ω)
     ((x) x)
     ((x y)
      (match (list x y)
	;;;(A⊔Bᶜ)∩(C⊔Dᶜ) = ∅ ⊔ (B∪D)ᶜ = X⊔Yᶜ, Y∖X=Y
        ((($ <set> a b)  ($ <set> c d))
	 (ifΩ (make-set ∅ (union b d))))

	;;; (A⊔Bᶜ) ∩ C = A∩C ∪ C∖B
	((($ <set> a b)  c)
	 (union (intersection a c) (difference c b)))

	;;; A ∩ (C⊔Dᶜ) = A∩C ∪ A∖D
	((a              ($ <set> c d))
	 (union (intersection a c) (difference a d)))
	
	;;; A ∩ B
	((a b)
	 (intersection a b))))

     ((x y . l)
      (nSS x (apply nSS y l)))))

  (define nSSo 
    (case-lambda
     (()  Ω)
     ((x) x)
     ((x y)
      (match (list x y)
	;;;(A⊔Bᶜ)∩(C⊔Dᶜ) = (A∩(C ∪ A∖D) ∪ C∖B) ⊔ (B∪D)ᶜ = X⊔Yᶜ, Y∖X=Y
        ((($ <set> a b)  ($ <set> c d))
	 (ifΩ 
	  (make-set (union (intersection (union c (difference a d)))
			   (difference c b)) 
		    (union b d))))

	;;; (A⊔Bᶜ) ∩ C = A∩C ∪ C∖B
	((($ <set> a b)  c)
	 (union (intersection a c) (difference c b)))

	;;; A ∩ (C⊔Dᶜ) = A∩(C ∪ A∖D)
	((a              ($ <set> c d))
	 (intersection a (union c (difference a d))))
	
	;;; A ∩ B
	((a b)
	 (intersection a b))))

     ((x y . l)
      (nSSo x (apply nSSo y l)))))

  (define (cS x)
    (match x
      (($ <set> a b)
       b)
      (x
       (cond
	((eq? x ∅)
	 Ω)
	((eq? x Ω)
	 ∅)
	(else
	 (ifΩ (make-set ∅ x)))))))

  (define (∖SS a b)
    ;; A∖B = A ∩ Bᶜ
    (nSS a (cS b)))

  (define (∖SSo a b)
    ;; A∖B = A ∩ Bᶜ
    (nSSo a (cS b)))

  (define ⊕SS
    (case-lambda
     (()    ∅)
     ((a)   a)
     ;; A ⊕ B = A∖B ∪ B∖A
     ((a b) (uSS (∖SS a b) (∖SS b a)))
     ((a b . l)
      (⊕SS a (apply ⊕SS b l)))))

  (define ⊕SSo
    (case-lambda
     (()    ∅)
     ((a)   a)
     ;; A ⊕ B = A∖B ∪ B∖A
     ((a b) (uSSo (∖SSo a b) (∖SSo b a)))
     ((a b . l)
      (⊕SSo a (apply ⊕SSo b l)))))

  (define (≡SS x y)
    (match (list x y)
       ((($ <set> x1 y1) ($ <set> x2 y2))
	(and (equiv? x1 x2) (equiv? y1 y2)))
       ((_ ($ <set>)) #f)
       ((($ <set>) ) #f)
       ((x y) (equiv? x y))))

  (define (⊆SS x y)
    (equiv? (nSS x y) x))

  (define (⊂SS x y)
    (and (not (equiv? x y)) (equiv? (nSS x y) x)))
  
  (values #:u  uSS  #:n  nSS  #:c  cS #:+  ⊕SS  #:-  ∖SS ; unordered operations
	  #:ou uSSo #:on nSSo #:oc cS #:o+ ⊕SSo #:o- ∖SS ; order     operations
	  #:world Ω #:= ≡SS #:< ⊂SS #:<= ⊆SS))

(define (lunion e x y)
  (append x
	  (let lp ((l '()) (y y))
	    (if (pair? y)
		(let ((xx (car y)))
		  (if (member xx x e)
		      (lp l (cdr y))
		      (lp (cons xx l) (cdr y))))
		(reverse l)))))
;; example
(define-syntax-parameter *set-equality-predicate* (lambda (x) #'equal?))
(define-syntax-parameter *set-empty*              (lambda (x) #''()))
(define-syntax-rule (m f) (lambda x (apply f *set-equality-predicate* x)))
(define skunk (lambda x #f))
(define-syntax with-tex-lset-operators
  (lambda (x)
    (syntax-case x ()
      ((name #:eq eq code ...)
       #'(syntax-parameterize ((*set-equality-predicate*
				(lambda (x) #'eq)))
	   (with-lset-operators code ...)))
       
      ((name code ...)       
       (with-syntax ((∪ (datum->syntax #'name '∪))
		     (∩ (datum->syntax #'name '∩))
		     (c (datum->syntax #'name 'c))
		     (⊕ (datum->syntax #'name '⊕))
		     (∖ (datum->syntax #'name '∖))
		     (≡ (datum->syntax #'name '≡))
		     (⊂ (datum->syntax #'name '⊂))
		     (⊆ (datum->syntax #'name '⊆)))
	 #'(call-with-values (lambda ()
			       (make-complementable-set *set-empty* 
							(m lunion)
							(m lset-intersection) 
							(m lset-difference)
							equal? length
							))
	     (lambda* (#:keys uo no co c +o -o = < <= )
		(define ∪ uo)
		(define ∩ no)
		(define c co)
		(define ⊕ +o)
		(define ∖ -o)
		(define ≡  =)
		(define ⊂  <)
		(define ⊆  <=)
		code ...)))))))

(define-syntax with-lset-operators
  (lambda (x)
    (syntax-case x ()
      ((name #:eq eq code ...)
       #'(syntax-parameterize ((*set-equality-predicate*
				(lambda (x) #'eq)))
	   (with-lset-operators code ...)))
       
      ((name code ...)       
       (with-syntax ((u  (datum->syntax #'name  'u))
		     (n  (datum->syntax #'name  'n))
		     (c  (datum->syntax #'name  'c))
		     (+  (datum->syntax #'name  '+))
		     (-  (datum->syntax #'name  '-))
		     (=  (datum->syntax #'name  '=))
		     (<  (datum->syntax #'name  '<))
		     (<= (datum->syntax #'name '<=)))
	 #'(call-with-values (lambda ()
			       (make-complementable-set *set-empty* 
							(m lunion)
							(m lset-intersection) 
							(m lset-difference)
							equal? length)) 
							
	     (lambda* (#:key uo no co +o -o = < <= #:allow-other-keys)
		(define u uo)
		(define n no)
		(define c co)
		(define + +o)
		(define - -o)
		code ...)))))))

(define asseq (lambda (eq) (lambda (x y) (eq (car x) (car y)))))

(define-syntax-parameter *set-map-equality-predicate* 
  (lambda (x) #'equal?))

(define-syntax-rule (with-lassoc-operators code ...)
  (syntax-parameterize ((*set-equality-predicate*
			 (lambda (x) #'(asseq *set-map-equality-predicate* ))))
    (with-lset-operators code ...)))

;;Bitsets are already complementable.
