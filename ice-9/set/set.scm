(define-module (ice-9 set set)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (<set> set? make-set-from-assoc make-set-from-assoc-mac set-size))

#|
This takes an assoc like library and transforms it to an ordered/undordered
set and setmap. There is a small change to the setup from scheme's assoc e.g.
the key value pair is not explicit in stead we assume the form (acons kv a).

It is a higher order library, it takes a set of functions and construct set opaerations ∪,∩,∖,≡,⊕ ordered and unordered.

Input:
(make-set-from-assoc-mac ...) the macro version of the below
(make-set-from-assoc 
 null assoc acons delete hash mk-kv size value? order? equal?)

null    =  the empty assoc/set
assoc   =  (assoc x set/map),  maps to either a key value pair or x itself if 
                               x is a member of set, else #f
acons   =  (acons x set/map),  funcitonally add x to set/map
delete  =  (delete x set/map), functionally delete x from set/map
size    =  (size set/map)    , >= the number of elements, if deleted elements
                               conses a delete operation to the assoc then this
                               is greater then the number of elements in the set
element operations

mk-kv   = (mk-kv x), makes an element from a normal scheme value  
hash    = (hash x hashsize), creates a hash value from the element
value?  = if this is a key-value pair and the order of the kv pairs in set 
          operations are important
order?  = if the element's construction order is reflected by the set and 
          maintaind through the ordering of the set
equal?  = (equal? x y) the equality predicate used for the elements.

Output: set operations and iteratables


(values #:=   ≡  #:u   u  #:n   n  #:-  s- #:+  s+ #:<   ⊂ #:<=   ⊆
	#:o=  o≡ #:ou  ou #:on  on #:o- o- #:o+ o+ #:o< o⊂ #:o<= o⊆
	#:fold fold #:map map #:for-each for-each #:empty ∅
	#:set->list set->list)

|#

(define-record-type <set>
  (make-set list assoc n hash)
  set?
  (list  set-list)
  (assoc set-assoc)
  (n     set-n)
  (hash  set-hash))

(define set-size set-n)

(set-record-type-printer! <set>
  (lambda (vl port) 
    (let ((n (set-n vl)))
      (if (= n 0)
	  (format port "∅")
	  (format port "#<set len=~a>" n)))))

(define-record-type <append>
  (make-append x y)
  append?
  (x append-x)
  (y append-y))

(define ö (list 'null-element))
(define (l-iter app)
  (let ((backlog '()))
    (lambda ()
      (let lp ()
	(match app
	  (($ <append> x y)
	   (set! backlog (cons y backlog))
	   (set! app x)
	   (lp))
	  ((x . l)
	   (set! app l)
	   x)
	  (()
	   (if (null? backlog)
	       ö
	       (begin
		 (set! app (car backlog))
		 (set! backlog (cdr backlog))
		 (lp)))))))))

(define (l-serie app)
  (let lp ((app app))
    (define (mapper x)
      (match x
        (($ <append> x y)
	 (append (lp x) (lp y)))
	(x (list x))))

    (define (mapq l)
      (match l
	((x  . l)
	 (append (mapper x) (mapq l)))
	(() '())
	(x (lp x))))

    (match app
       (($ <append> x y)
	(append (lp x) (lp y)))
       ((_ . _)
	(mapq  app))
       (x x))))

(define-syntax-rule (define-tool 
		      make-setfkns 
		      make-setfkns-mac
		      (args ...) code ...)
  (begin
    (define             (make-setfkns     args ...) code ...)
    (define-syntax-rule (make-setfkns-mac args ...) (begin code ...))))

(define-tool make-set-from-assoc make-set-from-assoc-mac
  (null assoc acons delete hash mk-kv kv-key kv-val sizefkn 
	value? order? equal?)


  (define ∅ (make-set '() null 0 0))  

  (define size 10000000000000)

  (define (make-one x)
    (if (set? x)
	x
	(let ((kv (mk-kv x)))
	  (make-set (list kv) (acons kv null) 1 (hash kv size)))))

  (define (set->* repr s)
    (match (make-one s)
      (($ <set> ll mm n h) 
       (let lp ((m null) (l '()) (ll (l-iter ll)))
	 (let ((kv (ll)))	      
	   (if (eq? kv ö)
	       (reverse l)
	       (let ((kv (assoc kv mm)))
		 (if (and kv (not (assoc kv m)))
		     (lp (acons kv m)            ;; hash
			 (cons (repr kv) l)      ;; li
			 ll)                    ;; itarator
		     (lp m l ll)))))))))

  (define (reprl x)  (value? x) x (kv-key x))
  (define (repra x)  (cons (kv-key x) (kv-val x)))
  (define (reprid x) x)

  (define set->list    (lambda (x) (set->* reprl x)))
  (define set->assoc   (lambda (x) (set->* repra x)))
  (define set->kvlist  (lambda (x) (set->* reprid x)))

 
  ;; Takes a sequence ll associated to assoc mm at length nn and 
  ;; produce a new Set which has all slack removed
  (define (do-truncate ll mm nn)
    (when (> (sizefkn mm) nn)
      (let lp ((m null) (i 0) (l '()) (ll (l-iter ll)) (h 0))
	(let ((kv (ll)))	      
	  (if (eq? kv ö)
	      (make-set (reverse l) m i h)
	      (let ((kv (assoc kv mm)))
		(if (and kv (not (assoc kv m)))
		  (lp (acons kv m)            ;; hash
		      (+ i 1)                 ;; len
		      (cons kv l)             ;; li
		      ll                      ;; itarator
		      (logxor h (hash kv size))) ;; h
		  (lp m i l ll h))))))))
  


  (define (maybe-truncate l m n h)
    (let ((nn (sizefkn m)))
      (if (> nn (* 2 n))
	  (do-truncate l m n)
	  (make-set    l m n h))))

  (define (≡ x y)
    (match (list (make-one x) (make-one y))
      ((($ <set> lx mx nx hx) ($ <set> ly my ny hy))
       (if (= hx hy)
	   (if (= nx ny)
	       (let lp ((lx (l-iter lx)))
		 (let ((kv (lx)))
		   (if (eq? kv ö)
		       #t
		       (let ((kvx (assoc kv mx)))
			 (if kvx
			     (let ((kvy (assoc kv my)))
			       (if kvy
				   (if (equal? kvx kvy)
				       (lp lx)
				       #f)
				   #f))
			     (lp lx))))))
	       #f)
	   #f))))

  (define (o≡ x y)
    (match (list (make-one x) (make-one y))
      ((($ <set> lx mx nx hx) ($ <set> ly my ny hy))
       (let ()
       (define (do-the-iteration)
	 (let lp ((lx (l-iter lx)) (mtx null) (ly (l-iter ly)) (mty ∅))
	   (let ((kvx (lx)))
	     (if (assoc kvx mtx)
		 (lp lx mtx ly mty)
		 (begin
		   (when (not (eq? kvx ö))
		     (let ((kvxx (assoc kvx mx)))
		       (if kvxx
			   (if (order? kvxx)
			       (set! kvx kvxx)
			       (let ((kvy (assoc kvxx my)))
				 (if kvy
				     (if (order? kvy)
					 (set! kvx kvxx)
					 (lp lx (assoc kvx mtx) ly mty))
				     #f)))
			   (lp lx mtx ly mty))))

		   (let lp2 ((ly ly) (mty mty))		     
		     (let ((kvy (ly)))
		       (if (assoc kvy mty)
			   (lp2 ly mty)
			   (begin
			     (when (not (eq? kvy ö))
				   (let ((kvyy (assoc kvy my)))
				     (if kvyy
					 (if (order? kvyy)
					     (set! kvy kvyy)
					     (let ((kvxx (assoc kvyy mx)))
					       (if kvxx
						   (if (equal? kvxx kvx)
						       (lp lx (acons kvx mtx)
							   ly (acons kvy mty))
						       (if (order? kvxx)
							   #f
							   (lp2 ly 
								(acons kvyy
								       mty))))
						   #f)))
					 (lp2 ly mty))))

			     (if (and (eq? kvx ö) (eq? kvy ö))
				 #t
				 (if (or (eq? kvx ö) (eq? kvy ö))
				     #f
				     (if (equal? kvx kvy)
					 (lp lx (acons kvx mtx)
					     ly (acons kvy mty))
					 #f))))))))))))


       (if (= hx hy)
	   (if (= nx ny)
	       (do-the-iteration)
	       #f)
	   #f)))))
	   

  (define (next-mute lp ll l m mt n h)
    (lp (cdr ll) l mt m n h))

  (define (next-skip lp ll l mt m n h kv)
    (lp (cdr ll) l (acons kv mt) m n h))

  (define (next-cons-kv-on-list lp ll l mt m n h kv kv*)
    (lp (cdr ll) (cons kv l) (acons kv mt) m n h))
  
  (define (next-add-kv lp ll l mt m n h kv kv*)
    (lp (cdr ll) (cons kv* l) (acons kv mt) (acons kv* m)
	(+ n 1) (logxor h (hash kv* size))))

  (define (next-add-l  lp ll l mt m n h kv*)
    (lp (cdr ll) (cons kv* l) (acons kv* mt) m n h))

  (define (next-swap-value lp ll l mt m n h kv kv* kv**)
    (lp (cdr ll) l (acons kv mt) (acons kv* m) n
	(logxor h 
		(logxor (hash kv*  size)
			(hash kv** size)))))

  (define (next-delete lp ll l mt m n h kv kv*)
    (lp (cdr ll) l
	(acons kv mt) 
	(delete kv* m) 
	(- n 1)
	(logxor h (hash kv* size))))

  ;;TODO MAKE USE OF APPEND CONSTRUCT MAKE USE OF VLISTS? MAYBE?
  (define-syntax-rule (mku ou is-ordered-set?)
  (define ou 
    (case-lambda
     (()   ∅)
     ((x)  (make-one x))
     ((x y)
      (let* ((x     (make-one x))
	     (y     (make-one y))
	     (nx    (set-n x))
	     (ny    (set-n y))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))
	(cond
	 ((= nx 0)
	  (if (= ny 0)
	      ∅
	      y))
	 ((= ny 0)
	  x)
	 ((< nx ny)
	  (let lp ((ll (l-serie lx))
		   (l  '()) (mt null) (m my) (n ny) (h (set-hash y)))
	      
	    (define-syntax-rule (next f . lll)
	      (f lp ll l mt m n h . lll))

	    (if (pair? ll)
		(let ((kv (car ll)))
		  (if (not (assoc kv mt))
		      (let ((kvx (assoc kv mx)))
			(if kvx
			    (let ((kvy (assoc kv my)))
			      (if (value? kvx)
				  (if kvy
				      (next next-swap-value kv kvx kvy)
				      (next next-add-kv kv kvx))
				  (if kvy
				      (if (order? kvx)
					  (next next-cons-kv-on-list kv kvx)
					  (next next-skip kv))
				      (next next-add-kv kv kvx))))
			    (next next-mute)))
		      (next next-mute)))
		(maybe-truncate (append (reverse l) ly)
				m n h))))
	  
	 (else
	  (let lp ((ll (l-serie ly))
		   (l  '()) (mt null) (m mx) (n nx) (h (set-hash x)))
	      
	    (define-syntax-rule (next f . lll)
	      (f lp ll l mt m n h . lll))
	      
	    (if (pair? ll)
		(let ((kv (car ll)))
		  (if (not (assoc kv mt))
		      (let ((kvx (assoc kv mx)))
			(if kvx
			    (let ((kvy (assoc kv my)))
			      (if kvy		  
				  (if (and (value? kvy) (not (value? kvx)))
				      (next next-swap-value kv kvy kvx)
				      (next next-skip kv))
				  (next next-mute)))
			    (let ((kvy (assoc kv my)))
			      (if kvy
				  (next next-add-kv kv kvy)
				  (next next-mute)))))
		      (next next-mute)))
		(maybe-truncate 
		 (make-append lx (reverse l)) m n h)))))))
     ((x y . l)
      (ou x (apply ou y l))))))

  (mku ou #t)
  (mku u  #f)

  (define-syntax-rule (mkn on is-ordered-set?)
  (define on 
    (case-lambda
     ((x)  x)
     ((x y)
      (let* ((x     (make-one x))
	     (y     (make-one y))
	     (nx    (set-n x))
	     (ny    (set-n y))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))

	(cond
	 ((or (= nx 0) (= ny 0))
	  ∅)

	 ((or (< nx 10) (< nx (* 2 ny)))
	  (let lp ((ll (l-serie lx)) (l '())
		   (mt null) (m mx) (n nx) (h (set-hash x)))

	      (define-syntax-rule (next f . lll) (f lp ll l mt m n h . lll))
	      (if (pair? ll)
		  (let ((kv (car ll)))
		    (if (not (assoc kv mt))
			(let ((kvx (assoc kv mx)))
			  (if kvx
			      (let ((kvy (assoc kv my)))
				(if kvy
				    (if (and (value? kvy) 
					     (not (value? kvx)))
					(next next-swap-value kv kvy kvx)
					(next next-skip kv))
				    (next next-delete kv kvx)))
			      (next next-skip kv)))
			(next next-mute)))		    
		  (maybe-truncate lx m n h))))
	 (else
	    (let lp ((ll (l-serie ly))
		     (l  ly)
		     (mt null) (m my) (n ny) (h (set-hash y)))
	      
	      (define-syntax-rule (next f . lll) (f lp ll l mt m n h . lll))
	      (if (pair? ll)
		  (let ((kv (car ll)))
		    (if (not (assoc kv mt))
			(let ((kvy (assoc kv my)))
			  (if kvy
			      (let ((kvx (assoc kv mx)))
				(if kvx
				    (if (value? kvx)
					(if (order? kvx)
					    (next next-swap-value kv kvx kvy)
					    (next next-skip kv))
					(next next-skip kv))
				    (next next-delete kv kvy)))
			      (next next-mute)))
			(next next-mute)))
		  (if (is-ordered-set?)
		      (maybe-truncate lx m n h)		   
		      (maybe-truncate ly m n h))))))))

     ((x y . l)
      (on x (apply on y l))))))

  (mkn on #t)
  (mkn n  #f)

 (define-syntax-rule (mk- o- is-ordered-set?)
  (define o- 
    (case-lambda
     ((x)  x)
     ((x y)
      (let* ((x     (make-one  x))
	     (y     (make-one  y))
	     (nx    (set-n     x))
	     (ny    (set-n     y))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))

	(cond
	 ((= nx 0)
	  ∅)
	 ((= ny 0)
	  x)
	 (else
	  (let lp ((ll (l-serie lx)) (l '())
		   (mt null) (m mx) (n nx) (h (set-hash x)))
	      
	    (define-syntax-rule (next f . lll) (f lp ll l mt m n h . lll))
	      
	    (if (pair? ll)
		(let ((kv (car ll)))
		  (if (not (assoc kv mt))
		      (let ((kvx (assoc kv mx)))
			(if kvx
			    (if (assoc kv my)
				(next next-delete kv kvx)
				(next next-skip kv))			  
			    (next next-mute)))
		      (next next-mute)))
		(maybe-truncate lx m n h)))))))
		
     ((x y . l)
      (apply o- (o- x y) l)))))

  (mk- o- #t)
  (mk- s- #f)

  (define o+ 
    (case-lambda
     (()  ∅)
     ((x) x)
     ((x y)
      (ou (o- x y) (o- y x)))
     ((x y . l)
      (o+ x (apply o+ y l)))))

  (define s+ 
    (case-lambda
     (()  ∅)
     ((x) x)
     ((x y)
      (u (s- x y) (s- y x)))
     ((x y . l)
      (s+ x (apply s+ y l)))))

  (define (fold f seed set)
    (let lp ((l  (l-serie (set-list set))) 
	     (m  (set-assoc set)))	     
      (let lp ((l l) (mt ∅) (seed seed))
	(if (pair? l)
	    (let ((kv (car l)))
	      (if (assoc kv mt)
		  (let ((kv (assoc kv m)))
		    (if kv
			(lp (cdr l) (acons kv mt) (f kv seed))
			(lp (cdr l) mt seed)))
		  (lp (cdr l) mt seed)))
	    seed))))

  (define (in x s)
    (match (make-one s)
     (($ <set> l m n h)
      (assoc (mk-kv x) m))))


  (define (⊆ x y)
    (≡ (n x y) x))


  (define (o⊆ x y)
    (o≡ (on y x) x))

  (define (⊂ x y)
    (and (not (≡ x y)) (⊆ x y)))

  (define (o⊂ x y)
    (and (not (≡ x y)) (⊆ x y)))

  (define (map f set) 
    (reverse (fold (lambda (k seed) (cons (f k) seed)) '() set)))
  (define (for-each f set) 
    (reverse (fold (lambda (k seed) (f k) seed) (if #f #f) set)))

  (values #:=   ≡  #:u   u  #:n   n  #:-  s- #:+  s+ #:<   ⊂ #:<=   ⊆
	  #:o=  o≡ #:ou  ou #:on  on #:o- o- #:o+ o+ #:o< o⊂ #:o<= o⊆
 	  #:in in #:fold fold #:map map #:for-each for-each #:empty ∅
	  #:set->list set->list #:set->assoc set->assoc 
	  #:set->kvlist set->kvlist))


